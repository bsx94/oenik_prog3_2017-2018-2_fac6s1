﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Prog3Beadando
{
    public abstract class Bindable : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;


        protected void OnPropertyChanged(string s = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(s));
        }


        protected void SetProperty<T>(ref T field, T value, [CallerMemberName] string s = "")
        {
            field = value;
            OnPropertyChanged(s);
        }
    }
}
