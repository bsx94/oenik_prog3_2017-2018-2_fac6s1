﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace Prog3Beadando
{
    /// <summary>
    /// Játékmotor
    /// </summary>
    class GameLogic
    {
        public bool IsGameOver { get; set; }
        bool spaceON;
        int pontszam;
        Size jatekmeret;
        List<Laser> xwingLaser;
        List<Laser> tie1Laser;
        List<Laser> tie2Laser;
        List<Bomb> tie2Bomb;
        List<Laser> turretLaser;
        public List<Enemies> EnemyList { get; set; }
        Xwing jatekos;
        public int Stage { get; set; }
        public bool newStage { get; set; }

        #region GETTER SETTER


        public int Pontszam
        {
            get
            {
                return pontszam;
            }

            set
            {
                pontszam = value;
            }
        }

        public Size Jatekmeret
        {
            get
            {
                return jatekmeret;
            }

            set
            {
                jatekmeret = value;
            }
        }





        public List<Laser> XwingLaser
        {
            get
            {
                return xwingLaser;
            }

            set
            {
                xwingLaser = value;
            }
        }

        public List<Laser> Tie1Laser
        {
            get
            {
                return tie1Laser;
            }

            set
            {
                tie1Laser = value;
            }
        }

        public List<Laser> TurretLaser
        {
            get
            {
                return turretLaser;
            }

            set
            {
                turretLaser = value;
            }
        }

        public Xwing Jatekos
        {
            get
            {
                return jatekos;
            }

            set
            {
                jatekos = value;
            }
        }

        public List<Laser> Tie2Laser
        {
            get
            {
                return tie2Laser;
            }

            set
            {
                tie2Laser = value;
            }
        }

        public List<Bomb> Tie2Bomb
        {
            get
            {
                return tie2Bomb;
            }

            set
            {
                tie2Bomb = value;
            }
        }

        public bool SpaceON
        {
            get
            {
                return spaceON;
            }

            set
            {
                spaceON = value;
            }
        }







        #endregion


        // Constructor

            /// <summary>
            /// VM constructor
            /// </summary>
            /// <param name="meret">Játéktér mérete</param>
        public GameLogic(Size meret)
        {
            jatekmeret = meret;
            SpaceON = false;
            Pontszam = 0;
            XwingLaser = new List<Laser>();
            Tie1Laser = new List<Laser>();
            Tie2Laser = new List<Laser>();
            Tie2Bomb = new List<Bomb>();
            TurretLaser = new List<Laser>();
            IsGameOver = false;
            Jatekos = new Xwing();
            EnemyList = new List<Enemies>();
            Stage = 0;
            newStage = true;
        }

        // Constructor

            /// <summary>
            /// Pályák közötti átmenet:
            /// Cím + visszaszámlálás
            /// </summary>
            /// <param name="sw">Stopperóra</param>
            /// <returns>Kiirandó szöveg</returns>
        public string Kiiras(Stopwatch sw)
        {
            if (Stage == 3 || Jatekos.Hp <= 0)
            {
                return GameOver();
            }
            else
            {
                sw.Start();
                if (sw.Elapsed.Seconds < 3)
                {
                    return "Stage " + (Stage + 1);
                }
                else if (sw.Elapsed.Seconds < 5)
                {
                    return "3";
                }
                else if (sw.Elapsed.Seconds < 7)
                {
                    return "2";
                }
                else if (sw.Elapsed.Seconds < 10)
                {
                    return "1";
                }
                else
                {
                    Stage++;
                    sw.Stop();
                    NewStage();
                    return "";
                }
            }




        }
        /// <summary>
        /// új pálya betöltése
        /// </summary>
        public void NewStage()
        {
            newStage = false;

            if (Stage == 1)
                Stage1();
            if (Stage == 2)
                Stage2();
            if (Stage == 3)
                Stage3();


        }

        /// <summary>
        /// Hogyan lett vége a játéknak? Win or Die?
        /// </summary>
        /// <returns>Kiirandó szöveg</returns>
        public string GameOver()
        {

            IsGameOver = true;
            if (Jatekos.Hp <= 0)
                return "Game Over";
            else
                return "You Win!";
        }



        /// <summary>
        /// Ellenségek mozgását indítja
        /// </summary>
        public void EllensegMozgas()
        {
            for (int i = 0; i < EnemyList.Count; i++)
            {


                if (EnemyList[i] is Turret)
                {
                    EnemyList[i].Mozgas(Jatekos.Center);


                }
                else if (EnemyList[i] is Tie1)
                {
                    EnemyList[i].Mozgas(Jatekmeret);

                }
                else if (EnemyList[i] is Tie2)
                {
                    EnemyList[i].Mozgas(Jatekmeret);

                }

            }
        }
        /// <summary>
        /// Ellenségek lövését indítja
        /// </summary>
        public void EllensegLoves()
        {
            for (int i = 0; i < EnemyList.Count; i++)
            {


                if (EnemyList[i] is Turret)
                    TurretLaser.Add(EnemyList[i].TurretFire(Jatekos.Center));
                else if (EnemyList[i] is Tie1)
                    Tie1Laser.Add(EnemyList[i].TieFire());
                else if (EnemyList[i] is Tie2)
                {
                    Tie2Laser.Add(EnemyList[i].Tie2Fire());
                    Tie2Bomb.Add(EnemyList[i].TieBomb(Jatekos.Center));
                }

            }
        }


        /// <summary>
        /// Találatok vizsgálata és lövedék eltávolítása, ha elhagyja a pályát.
        /// Tickenként 1 HP visszatöltés.
        /// </summary>
        public void Update()
        {


            #region Xwing Lövései

            for (int j = 0; j < XwingLaser.Count; j++)
            {
                if (XwingLaser[j].Palyan(Jatekmeret)) //pályán van, de ütközik-e?
                {
                    Rect laserRect = XwingLaser[j].Area;

                    #region Lézert talált
                    //Turret
                    for (int i = 0; i < TurretLaser.Count; i++)
                    {
                        if (laserRect.IntersectsWith(TurretLaser[i].Area)) //találkoznak
                        {
                            TurretLaser.Remove(TurretLaser[i]);
                            XwingLaser.Remove(XwingLaser[j]);
                            Pontszam++;
                        }
                    }


                    //T1
                    for (int i = 0; i < Tie1Laser.Count; i++)
                    {
                        if (laserRect.IntersectsWith(Tie1Laser[i].Area)) //találkoznak
                        {
                            XwingLaser.Remove(XwingLaser[j]);
                            Tie1Laser.Remove(Tie1Laser[i]);
                            Pontszam++;
                        }

                    }


                    //T2
                    for (int i = 0; i < Tie2Laser.Count; i++)
                    {
                        if (laserRect.IntersectsWith(Tie2Laser[i].Area)) //találkoznak
                        {
                            XwingLaser.Remove(XwingLaser[j]);
                            Tie2Laser.Remove(Tie2Laser[i]);
                            Pontszam++;
                        }
                    }

                    #endregion

                    //Ellenséget talált-e?
                    for (int i = 0; i < EnemyList.Count; i++)
                    {
                        if (laserRect.IntersectsWith(EnemyList[i].Area)) //találkoznak
                        {
                            EnemyList[i].Hp -= 5;
                            XwingLaser.Remove(XwingLaser[j]);
                            Pontszam++;

                            if (EnemyList[i].Hp <= 0)
                            {
                                EnemyList.Remove(EnemyList[i]);
                                Pontszam += 10;
                            }
                        }
                    }
                }
                else //Elhagyta a pályát
                    XwingLaser.Remove(XwingLaser[j]);
            }

            #endregion
            #region Ellenség Lövései



            Rect xwingrect = Jatekos.Area;


            // TURRET
            for (int j = 0; j < TurretLaser.Count; j++)
            {
                if (TurretLaser[j].Palyan(Jatekmeret)) //pályán van, de ütközik-e?
                {
                    if (TurretLaser[j].Area.IntersectsWith(xwingrect)) //találkoznak
                    {
                        Jatekos.Hp -= 50;
                        Pontszam--;
                        TurretLaser.Remove(TurretLaser[j]);
                    }
                }
                else
                    TurretLaser.Remove(TurretLaser[j]);
            }

            //TIE1

            for (int j = 0; j < Tie1Laser.Count; j++)
            {
                if (Tie1Laser[j].Palyan(Jatekmeret)) //pályán van, de ütközik-e?
                {

                    if (Tie1Laser[j].Area.IntersectsWith(xwingrect)) //találkoznak
                    {
                        Jatekos.Hp -= 50;
                        Pontszam--;
                        Tie1Laser.Remove(Tie1Laser[j]);
                    }
                }
                else
                    Tie1Laser.Remove(Tie1Laser[j]);
            }

            //TIE2

            for (int j = 0; j < Tie2Laser.Count; j++)
            {
                if (Tie2Laser[j].Palyan(Jatekmeret)) //pályán van, de ütközik-e?
                {

                    if (Tie2Laser[j].Area.IntersectsWith(xwingrect)) //találkoznak
                    {
                        Jatekos.Hp -= 50;
                        Pontszam--;
                        Tie2Laser.Remove(Tie2Laser[j]);
                    }
                }
                else
                    Tie2Laser.Remove(Tie2Laser[j]);
            }


            for (int j = 0; j < Tie2Bomb.Count; j++)
            {
                if (Tie2Bomb[j].Palyan(Jatekmeret, Jatekos.Center)) //pályán van, de ütközik-e?
                {

                    if (Tie2Bomb[j].Area.IntersectsWith(xwingrect)) //találkoznak
                    {
                        Jatekos.Hp -= 50;
                        Pontszam--;
                        Tie2Bomb.Remove(Tie2Bomb[j]);
                    }
                }
                else
                    Tie2Bomb.Remove(Tie2Bomb[j]);
            }




            #endregion





            if (Jatekos.Hp < 1000)
                Jatekos.Hp += 1;
        }
        //UpdateEnd

            /// <summary>
            /// Pályák.
            /// </summary>
        #region Stages
        void Stage1()
        {

            EnemyList.Add(new Turret(new Point(Jatekmeret.Width - 200, Jatekmeret.Height / 2)));

        }
        void Stage2()
        {
            Pontszam += 100;
            EnemyList.Add(new Turret(new Point(Jatekmeret.Width - 200, 100)));
            EnemyList.Add(new Turret(new Point(Jatekmeret.Width - 200, Jatekmeret.Height - 200)));
            EnemyList.Add(new Tie1(Jatekmeret));

        }
        void Stage3()
        {
            Pontszam += 100;

            EnemyList.Add(new Tie2(Jatekmeret));

        }
        #endregion


        /// <summary>
        /// Irányítás
        /// </summary>
        /// <param name="gomb">Bejövő parancs</param>
        public void BillentyuNyomas(Key gomb)
        {
            if (gomb == Key.Up && (Jatekos.Center.Y - 10) >= 100)
            {
                // Xwing FEL               
                Jatekos.Center = new Point(Jatekos.Center.X, Jatekos.Center.Y - 10);
                Jatekos.Area = new Rect(Jatekos.Center.X, Jatekos.Center.Y, 100, 100);
            }
            else if (gomb == Key.Down && (Jatekos.Center.Y + 20) <= (Jatekmeret.Height - 200))
            {
                // Xwing LE
                Jatekos.Center = new Point(Jatekos.Center.X, Jatekos.Center.Y + 20);
                Jatekos.Area = new Rect(Jatekos.Center.X, Jatekos.Center.Y, 100, 100);
            }
            else if (gomb == Key.Space)
            {
                if (!SpaceON)
                {
                    double x = 15;
                    double y = 0;

                    XwingLaser.Add(new Laser(
                        new Point(Jatekos.Center.X + 80, Jatekos.Center.Y + 50 - 40),
                        new Vector(x, y),
                        Jatekos.Fire));



                    XwingLaser.Add(new Laser(
                        new Point(Jatekos.Center.X + 80,
                        Jatekos.Center.Y + 50 + 40), new Vector(x, y),
                        Jatekos.Fire));

                    SpaceON = true;
                }




            }
        }
    }
}
