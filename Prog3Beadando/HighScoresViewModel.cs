﻿namespace Prog3Beadando
{
    /// <summary>
    /// Rekordokhoz tartozó VM
    /// </summary>
    public class HighScoresViewModel
    {
        string[] highScores;
        public HighScoresViewModel()
        {

        }

        public string[] HighScores
        {
            get
            {
                return highScores;
            }

            set
            {
                highScores = value;
            }
        }
    }
}