﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Prog3Beadando
{
    /// <summary>
    /// Interaction logic for GameWindow.xaml
    /// </summary>
    public partial class GameWindow : Window
    {
        GameLogic VM;
        DispatcherTimer dt;
        DispatcherTimer ellensegtimer;
        bool firstTick { get; set; }
        public GameWindow()
        {
            InitializeComponent();
            firstTick = true;
        }

        private void GameOver()
        {
            dt.Stop();
            ellensegtimer.Stop();

            new GameOver(new GameOverViewModel(VM.Pontszam)).Show();
            this.Close();
        }
        private void Dt_Tick(object sender, EventArgs e)
        {
            VM.EllensegMozgas();
            VM.Update();

            if (VM.EnemyList.Count == 0 || VM.Jatekos.Hp <= 0)
                VM.newStage = true;

            if (VM.IsGameOver)
                GameOver();

            gamescreen.InvalidateVisual();


        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            VM = new GameLogic(new Size(gamescreen.ActualWidth, gamescreen.ActualHeight));

            gamescreen.Init(VM);
            this.KeyDown += Window_KeyDown;




            dt = new DispatcherTimer();
            dt.Interval = TimeSpan.FromMilliseconds(20);
            dt.Tick += Dt_Tick;
            dt.Start();

            

            ellensegtimer = new DispatcherTimer();
            ellensegtimer.Interval = TimeSpan.FromMilliseconds(750);
            ellensegtimer.Tick += Ellensegtimer_Tick;
            ellensegtimer.Start();




        }

       

        /// <summary>
        /// Lövések itt jönnek létre
        /// ez egy lassabb DispatcherTimer, hogy ne folytonos vonalban jöjjön a tűz.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Ellensegtimer_Tick(object sender, EventArgs e)
        {
            VM.EllensegLoves();
            gamescreen.InvalidateVisual();
        }

        private void VM_jatekvege(object sender, EventArgs e)
        {
            MessageBoxResult mbr = MessageBox.Show("Játék vége...");
            if (mbr == MessageBoxResult.OK)
            {

                this.Close();
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            VM.BillentyuNyomas(e.Key);
            if (e.Key == Key.Escape)
            {
                this.Close();
            }
            gamescreen.InvalidateVisual();
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                VM.SpaceON = false;
            }
        }
    }
}
