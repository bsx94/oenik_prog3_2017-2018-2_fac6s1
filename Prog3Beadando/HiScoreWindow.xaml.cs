﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Prog3Beadando
{
    /// <summary>
    /// Interaction logic for HiScoreWindow.xaml
    /// </summary>
    public partial class HiScoreWindow : Window
    {
        HighScoresViewModel vm;
        public HiScoreWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            vm = new HighScoresViewModel();
            vm.HighScores = new string[10];
            List<string> lines;
            try
            {
                lines = File.ReadAllLines("highscores.txt").ToList();
            }
            catch (FileNotFoundException)
            {
                lines = new List<string>();
            }
            List<string> scores = new List<string>();
            while (lines.Count != 0 && scores.Count != 10)
            {
                List<int> values = new List<int>();
                foreach (string line in lines)
                {
                    values.Add(int.Parse(line.Split(' ')[1]));
                }
                if (values.Count != 0 && lines.Count != 0)
                {
                    int maxIndex = values.IndexOf(values.Max());
                    scores.Add(lines[maxIndex]);
                    lines.RemoveAt(maxIndex);
                }
            }
            for (int i = 0; i < scores.Count; i++)
            {
                vm.HighScores[i] = scores[i].Split(' ')[0] + " - " + scores[i].Split(' ')[1] + " pont";
            }

            this.DataContext = vm;
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            new MainWindow().Show();
            Close();
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
