﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Prog3Beadando
{
    /// <summary>
    /// Megjelenítésért felelős osztály
    /// </summary>
    class GameScreen : FrameworkElement
    {

        Stopwatch stopwatch { get; set; }
        ImageBrush hatter;
        GameLogic VM;
        public GameScreen()
        {
        }

        public void Init(GameLogic vm)
        {
            VM = vm;
            stopwatch = new Stopwatch();
            hatter = new ImageBrush(
                     new BitmapImage(new Uri
                     ("img/bg.png", UriKind.RelativeOrAbsolute)
                     ));

        }


        
        /// <summary>
        /// Tickenként kirajzolja a VM-túl kapott dolgokat
        /// </summary>
        /// <param name="drawingContext"></param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (VM != null)
            {



                // HÁTTÉR KIRAJZOLÁSA
                drawingContext.DrawRectangle(
                    hatter,
                    null,
                    new Rect(0, 0, this.ActualWidth, this.ActualHeight)
                    );

                //Felirat

                drawingContext.DrawText(new FormattedText(
                    "Pontszám: " + VM.Pontszam,
                    System.Globalization.CultureInfo.CurrentCulture,
                    FlowDirection.LeftToRight,
                    new Typeface("Arial"),
                    36,
                    Brushes.Yellow),
                    new Point(VM.Jatekmeret.Width / 2 - 100, VM.Jatekmeret.Height - 70)
                    );

                //Hp
                SolidColorBrush b;
                if (VM.Jatekos.Hp > 75)
                {
                    b = Brushes.Lime;
                }
                else if (VM.Jatekos.Hp > 35)
                {
                    b = Brushes.Yellow;
                }
                else
                {
                    b = Brushes.Red;
                }


                drawingContext.DrawRectangle(
                    Brushes.DarkGray,
                    new Pen(Brushes.Black, 2),
                    new Rect(50, VM.Jatekmeret.Height - 50, 300, 20)
                    );


                if (VM.Jatekos.Hp > 0)
                    drawingContext.DrawRectangle(
                        b,
                        new Pen(Brushes.Black, 2),
                        new Rect(50, VM.Jatekmeret.Height - 50, (VM.Jatekos.Hp / 10) * 3, 20)
                        );


                // Xwing
                #region XWING KIRAJZOLÁSA


                drawingContext.DrawRectangle(
                    VM.Jatekos.Kep,
                    null,
                    VM.Jatekos.Area
                    );

                // LÉZEREK KIRAJZOLÁSA

                foreach (Laser item in VM.XwingLaser)
                {

                    drawingContext.DrawRectangle(
                    item.Kep,
                    null,
                    item.Area
                    );


                }
                #endregion
                //Ellenség Lézer
                #region Ellenség Lézer KIRAJZOLÁSA



                foreach (Laser item in VM.TurretLaser)
                {
                    drawingContext.DrawRectangle(
                        item.Kep,
                        null,
                        item.Area
                        );
                }


                foreach (Laser item in VM.Tie1Laser)
                {
                    drawingContext.DrawRectangle(
                    item.Kep,
                    null,
                    item.Area
                    );
                }

                foreach (Laser item in VM.Tie2Laser)
                {
                    drawingContext.DrawRectangle(
                        item.Kep,
                        null,
                        item.Area
                        );
                }
                foreach (Bomb item in VM.Tie2Bomb)
                {
                    drawingContext.DrawRectangle(
                        item.Kep,
                        null,
                        item.Area
                        );
                }
                #endregion
                //Ellenség
                #region Ellenségek Kirajzolása



                foreach (Enemies item in VM.EnemyList)
                {
                    drawingContext.DrawRectangle(
                        item.Kep,
                        null,
                        item.Area
                        );

                }
                #endregion
                // Átvezető
                if (VM.newStage)
                {

                    stopwatch.Start();
                    FormattedText ft = new FormattedText(
                                VM.Kiiras(stopwatch),
                                System.Globalization.CultureInfo.CurrentCulture,
                                FlowDirection.LeftToRight,
                                new Typeface("Impact"),
                                300,
                                Brushes.Red)
                                ;
                    ft.TextAlignment = TextAlignment.Center;
                    drawingContext.DrawText(ft,
                                new Point((VM.Jatekmeret.Width / 2), (VM.Jatekmeret.Height / 2) - 200)
                                );
                }
                else
                {
                    stopwatch.Stop();
                    stopwatch.Reset();
                }



                //OnRenderEnd
            }
        }
    }
}
