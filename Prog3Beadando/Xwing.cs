﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Prog3Beadando
{
    /// <summary>
    /// Irányított űrcirkáló
    /// </summary>
    class Xwing : Fighter
    {


        public Xwing()
        {
            Hp = 1000;
            Center = new Point(100, 100);
            Area = new Rect(Center.X, Center.Y, 100, 100);
            Fire = new ImageBrush(
                new BitmapImage(new Uri
                ("img/xwinglaser.png", UriKind.RelativeOrAbsolute)
                ));

            Kep = new ImageBrush(
                new BitmapImage(new Uri
                ("img/xwing.png", UriKind.RelativeOrAbsolute)
                ));
        }




    }
}
