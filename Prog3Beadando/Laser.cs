﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Prog3Beadando
{
    /// <summary>
    /// Lövedékek tulajdonságai, metódusai
    /// </summary>
    class Laser : GameObjects
    {




        public Laser(Point ctr, Vector spd, Rect A, ImageBrush pict)
        {
            Center = ctr;
            Speed = spd;
            Area = A;
            Kep = pict;

        }

        public Laser(Point ctr, Vector spd, ImageBrush pict)
        {

            Center = ctr;
            Speed = spd;
            Area = new Rect(Center.X, Center.Y, 30, 8);
            Kep = pict;
        }


        /// <summary>
        /// Lövedék mozgatása,ha pályán van
        /// ha nincs VM törli majd
        /// </summary>
        /// <param name="jatekter"></param>
        /// <returns></returns>
        public bool Palyan(Size jatekter)
        {
            //hova kerülne a lövedék
            Point newcenter = new Point(Center.X + Speed.X, Center.Y + Speed.Y);
            if (newcenter.X >= 0 && //bal
                newcenter.Y >= 0 && //fent
                newcenter.X <= jatekter.Width && //jobb
                newcenter.Y <= jatekter.Height) //lent
            {
                //pályán belül van -> áthelyezzük, igaz visszatérés

                Center = newcenter;
                Area = new Rect(Center.X, Center.Y, Area.Width, Area.Height);
                return true;
            }
            else
            {
                //elhagyta a pályát -> hamis visszatérés, VM törli a lövedéket
                return false;
            }
        }
    }

    /// <summary>
    /// Specko lövedék Tie2 részére
    /// </summary>
    class Bomb : Laser
    {
        public Bomb(Point ctr, Vector spd, ImageBrush pict) : base(ctr, spd, pict)
        {
            Area = new Rect(Center.X, Center.Y, 20, 8);
            Kep = pict;


        }

        /// <summary>
        /// Mozgás nyomkövetéssel
        /// </summary>
        /// <param name="jatekter">játéktér mérete</param>
        /// <param name="target">célpont</param>
        /// <returns></returns>
        public bool Palyan(Size jatekter, Point target)
        {
            double y;
            //hova kerülne a lövedék
            if (target.Y + 50 > Center.Y) // ha lentebb van            
                y = 1;
            else if (target.Y + 50 < Center.Y)
                y = -1;
            else y = 0;
            Point newcenter = new Point(Center.X + Speed.X, Center.Y + Speed.Y + y * 5);
            if (newcenter.X >= 0 && //bal
                newcenter.Y >= 0 && //fent
                newcenter.X <= jatekter.Width && //jobb
                newcenter.Y <= jatekter.Height) //lent
            {
                //pályán belül van -> áthelyezzük, igaz visszatérés
                
                Center = newcenter;
                Area = new Rect(Center.X, Center.Y, Area.Width, Area.Height);
                return true;
            }
            else
            {
                //elhagyta a pályát -> hamis visszatérés, VM törli a lövedéket
                return false;
            }
        }
    }




}
