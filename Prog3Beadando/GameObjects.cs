﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Prog3Beadando
{
    /// <summary>
    /// Játék objektumok ősosztálya.
    /// Közös tulajdonságok örökítése
    /// </summary>
    abstract class GameObjects
    {
        public Random rand = new Random();
        protected int hp;
        private Vector speed;
        private Point center;
        Rect area;
        private ImageBrush kep;


        public ImageBrush Kep
        {
            get
            {
                return kep;
            }

            set
            {
                kep = value;
            }
        }


        public Rect Area
        {
            get
            {
                return area;
            }

            set
            {
                area = value;
            }
        }

        

        public int Hp
        {
            get
            {
                    return hp;
            }

            set
            {
                hp = value;
            }
        }

       

        public Point Center
        {
            get
            {
                return center;
            }

            set
            {
                center = value;
            }
        }

        public Vector Speed
        {
            get
            {
                return speed;
            }

            set
            {
                speed = value;
            }
        }
         

    }
}
