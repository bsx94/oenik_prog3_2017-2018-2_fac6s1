﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prog3Beadando
{
    /// <summary>
    /// Gameover ablakhoz tartozó viewmodel a rekordok mentéséhez
    /// </summary>
    public class GameOverViewModel : Bindable
    {
        string playerName;
        string playerScore;

        public string PlayerName
        {
            get { return this.playerName; }
            set
            {
                SetProperty(ref playerName, value);
            }
        }

        public string PlayerScore
        {
            get { return this.playerScore; }
            set
            {
                SetProperty(ref playerScore, value);
            }
        }

        

        public GameOverViewModel(int score)
        {
            PlayerScore = score.ToString();
            OnPropertyChanged();
        }

        public GameOverViewModel()
        {
                
        }
    }
}
