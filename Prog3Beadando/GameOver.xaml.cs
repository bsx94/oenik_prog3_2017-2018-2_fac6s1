﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Prog3Beadando
{
    /// <summary>
    /// Játékvége ablak
    /// </summary>
    public partial class GameOver : Window
    {
        GameOverViewModel vm;
        public GameOver(GameOverViewModel vm)
        {
            InitializeComponent();
            this.vm = vm;
            DataContext = vm;
        }

        private void Kilepes()
        {


            if (!string.IsNullOrWhiteSpace(vm.PlayerName))
            {
                if (vm.PlayerName == "" || vm.PlayerName == " ")
                {
                    vm.PlayerName = "Senki";
                }
                string[] lines;
                try
                {
                    lines = File.ReadAllLines("highscores.txt");
                }
                catch (FileNotFoundException)
                {
                    lines = new string[0];
                }
                string newLine = vm.PlayerName + " " + vm.PlayerScore;
                string[] newLines = new string[lines.Length + 1];
                for (int i = 0; i < lines.Length; i++)
                {
                    newLines[i] = lines[i];
                }
                newLines[lines.Length] = newLine;
                File.WriteAllLines("highscores.txt", newLines);

                new MainWindow().Show();


            }
            else
            {

                switch (MessageBox.Show("Szeretnél Új játékot indítani?", "Kilépés", MessageBoxButton.YesNo))
                {
                    case MessageBoxResult.None:
                        new MainWindow().Show();
                        break;
                    case MessageBoxResult.Yes:
                        new GameWindow().Show();
                        break;
                    case MessageBoxResult.No:
                        break;
                    default:
                        break;
                }

            }


        }

        private void MainMenu_Click(object sender, RoutedEventArgs e)
        {

            this.Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Kilepes();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            new GameWindow().Show();

        }
    }
}
