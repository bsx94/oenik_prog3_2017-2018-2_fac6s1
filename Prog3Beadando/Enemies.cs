﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Prog3Beadando
{
    /// <summary>
    /// Ellenségek metódusai, tulajdonságai
    /// </summary>
    class Enemies : Fighter
    {
        #region TieMethods
        /// <summary>
        /// Egyenes lövések
        /// </summary>
        /// <returns>Tie1 lövedék</returns>
        public Laser TieFire()
        {
            return new Laser(new Point(Center.X + 40, Center.Y + 50),
                   new Vector(-15 + rand.Next(-2, 2), rand.Next(-2, 2)),
                   Fire
                   );
        }
        /// <summary>
        /// Egyenes lövések
        /// </summary>
        /// <returns>Tie2 lövedék</returns>
        public Laser Tie2Fire()
        {
            return new Laser(new Point(Center.X + 20, Center.Y + 30),
                   new Vector(-15 + rand.Next(-2, 2), rand.Next(-2, 2)),
                   Fire);
        }
        /// <summary>
        /// Nyomkövető lövedék
        /// </summary>
        /// <param name="target"> Célpont helye </param>
        /// <returns>Tie2 bomba</returns>
        public Bomb TieBomb(Point target)
        {
            return new Bomb(new Point(Center.X + 20, Center.Y + 70),
                            new Vector(-10 + rand.Next(-2, 2), target.Y / 100),
                            new ImageBrush(
                                            new BitmapImage(new Uri
                                            ("img/bomb.png", UriKind.RelativeOrAbsolute)
                                          )
                            )
                   );
        }
        /// <summary>
        /// Területen belüli mozgás
        /// </summary>
        /// <param name="mezo">terület mérete</param>
        public void Mozgas(Size mezo)
        {
            Point newcenter = new Point(Center.X + Speed.X, Center.Y + Speed.Y);

            if (newcenter.X >= (mezo.Width / 2) &&
                newcenter.X <= (mezo.Width - 100) &&
                newcenter.Y >= 0 &&
                newcenter.Y <= (mezo.Height - 100))
            {
                this.Center = newcenter;
            }
            else
            {
                if (Center.X + Speed.X <= mezo.Width / 2) //bal
                {
                    Speed = new Vector(-1 * Speed.X, Speed.Y);
                }

                if (Center.Y + Speed.Y <= 0) //fent
                {
                    Speed = new Vector(Speed.X, -1 * Speed.Y);
                }

                if (Center.X + Speed.X >= mezo.Width - 100) //jobb
                {
                    Speed = new Vector(-1 * Speed.X, Speed.Y);
                }

                if (Center.Y + Speed.Y >= mezo.Height - 100) //lent
                {
                    Speed = new Vector(Speed.X, -1 * Speed.Y);
                }

                Center = new Point(Center.X + Speed.X, Center.Y + Speed.Y);
            }

            this.Area = new Rect(Center.X, Center.Y, 100, 100);


        }

        #endregion

        #region TurretMetodhs
        /// <summary>
        /// Forgó mozgás
        /// </summary>
        /// <param name="target">Célpont helye</param>
        public void Mozgas(Point target) //turret
        {
            double a = Center.Y - target.Y;
            double b = target.X - Center.X;

            double fordulas = Math.Atan2(b, a) * (180 / Math.PI) + 90;

            Kep.Transform = new RotateTransform(fordulas, Center.X + 50, Center.Y + 50);

        }

        /// <summary>
        /// Célpont irányába indítja a lövedéket
        /// </summary>
        /// <param name="target">Célpont</param>
        /// <returns>Turret lövedék</returns>
        public Laser TurretFire(Point target) //turret
        {


            return new Laser(new Point(Center.X + 50, Center.Y + 50),
                   new Vector((target.X + 50 - (Center.X + 50) + rand.Next(-15, 15)) / 100,
                   (target.Y + 50 - (Center.Y + 50) + rand.Next(-15, 15)) / 100),
                   new Rect(Center.X + 50, Center.Y + 50, 15, 15),
                   Fire
                   );
        }
        #endregion

    }

    /// <summary>
    /// A TIE vadász a Galaxis egyik legelterjedtebb csillagvadásza, a rohamosztagosok és a csillagrombolók mellett pedig a Galaktikus Birodalom hatalmának és katonai erejének jelképe volt. A Sienar Fleet Systems tervezőirodáiban megszületett vadász nem rendelkezett sem páncélzattal, sem életfenntartó berendezésekkel, de nagy sebességének, kiváló manőverezőképességének és persze félelmetes megjelenésének köszönhetően minden típusú vadászgéppel fel tudta venni a versenyt.
    /// </summary>
    class Tie1 : Enemies
    {
        /// <summary>
        /// Tie constructor
        /// értékek beállítása
        /// </summary>
        /// <param name="mezo">Területen belüli véletlenszerű elhelyezés</param>
        public Tie1(Size mezo)
        {
            Hp = 50;
            Fire = new ImageBrush(
                new BitmapImage(new Uri
                ("img/tie1laser.png", UriKind.RelativeOrAbsolute)
                ));
            Center = new Point((int)rand.Next((int)mezo.Width / 2 + 100, (int)mezo.Width) - 100, (int)rand.Next(100, (int)mezo.Height - 100));
            int r = 0;
            do
            {
                r = rand.Next(-15, 15);
            }
            while (r == 0);


            Speed = new Vector(r, r);
            Area = new Rect(Center.X, Center.Y, 100, 100);
            Kep = new ImageBrush(
                new BitmapImage(new Uri
                ("img/tie1.png", UriKind.RelativeOrAbsolute)
                ));
        }





    }
    /// <summary>
    /// A Sienar Fleet Systems gyártmánya. A légkörben 850 km-es sebességgel repül, az űrben pedig mindössze 60 MGLT-vel, ezzel borzasztóan lomha. Hiperhajtóműve nincs. Személyzetnek mindössze egyetlen pilóta kell. Felépítése „dupla konstrukció”, azaz két „rekesz” található meg a két "(" alakú szárny között. Az egyik a pilótafülke, a másik a teherhordó, ahol akár 15 tonna felszerelést is lehet szállítani. Két ionhajtóműve van, egy-egy mindkét rekeszen. 7.8 méter hosszúságú. A fegyverzetről egy-két szó. Jól fel van velük szerelve, 2 lézerágyú, 2 hőkövető kilövő, 2 protontorpedó kilövő, valamint egy bombarekesz található meg rajta. Ezt általában proton, vagy ionbombával szokták megtölteni. Mindhárom ágyú Sienar gyártmányú. A 2 db L-s1 típusú lézerágyú, a két gömbön helyezkedik el. A 2 hőkövető kilövőben 4-4 rakéta fér el, ezek M-s3 típusúak. A protontorpedókból 2-2 fér be a kilövőkbe, amelyek szériaszáma T-s5.
    /// </summary>
    class Tie2 : Enemies
    {

        public Tie2(Size jatekter)
        {
            Hp = 500;
            Fire = new ImageBrush(
                new BitmapImage(new Uri
                ("img/tie1laser.png", UriKind.RelativeOrAbsolute)
                ));
            Center = new Point(2 * jatekter.Width / 3, jatekter.Height / 2);

            int r = 0;
            do
            {
                r = rand.Next(-10, 10);
            }
            while (r == 0);


            Speed = new Vector(r, r);
            Area = new Rect(Center.X, Center.Y, 100, 100);
            Kep = new ImageBrush(
                new BitmapImage(new Uri
                ("img/tie2.png", UriKind.RelativeOrAbsolute)
                ));
        }
    }

    /// <summary>
    /// forgó mozgást végző ágyú
    /// </summary>
    class Turret : Enemies
    {
        public Turret(Point ctr)
        {
            Hp = 200;
            Fire = new ImageBrush(
                new BitmapImage(new Uri
                ("img/turretlaser.png", UriKind.RelativeOrAbsolute)
                ));
            Center = ctr;
            Area = new Rect(Center.X, Center.Y, 100, 100);
            Kep = new ImageBrush(
                new BitmapImage(new Uri
                ("img/turret.png", UriKind.RelativeOrAbsolute)
                ));

        }


    }

}
